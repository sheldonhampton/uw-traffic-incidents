# README #

This is the code backing the POC for the UW VCC Traffic Incidents project. It is comprised of one React App, one Go lambda, and one Go executable. The executable merges the source csv files and updates their schemas to a common mapping. The output is crawled by an AWS Glue Crawler. A Glue Job is then used to update an Athena database that the lambda function will query after being triggered by Api Gateway. Most of what you'd need for minimum success is hard coded or clear as mud once you get into the AWS console. If any help is needed feel free to reach out to Sheldon Hampton II (2062516759,sheldon.hampton@parivedasolutions.com,tharivolnailo@protonmail.com)

The full data set used is in the Pariveda Box account. The current csvs in this project are jsut the first lines of those files to make testing easier.

beware the date formats.
