package main

import (
	"log"
	"regexp"
	"strconv"
	"strings"
	"time"
)

const kcmTimeFormat = "01/02/2006 02:04"
const sfdTimeFormat = "01/02/2006 02:04"
const spdTimeFormat = "01/02/2006 02:04"
const kcmZeroHourTimeFormat = "01/02/2006 00:04"
const sfdZeroHourTimeFormat = "01/02/2006 00:04"
const spdZeroHourTimeFormat = "01/02/2006 00:04"
const spdArrivalTimeFormat = "01/02/2006 02:02:02"
const spdArrivalZeroHourTimeFormat = "01/02/2006 00:02:02"

func MapKcmToCommon(headers []string) map[string]int {
	return mapToCommon(headers, kcmMap)
}

func MapSfdToCommon(headers []string) map[string]int {
	return mapToCommon(headers, sfdMap)
}

func MapSpdToCommon(headers []string) map[string]int {
	return mapToCommon(headers, spdMap)
}

func MapWspToCommon(headers []string) map[string]int {
	return mapToCommon(headers, wspMap)
}

func Map(headerIndexMap map[string]int, headers, data []string, f func(string) (int64, bool)) []string {
	out := make([]string, len(headers))
	for i, header := range headers {
		val, ok := headerIndexMap[strings.ToLower(header)]
		if !ok {
			continue
		}

		var datum string
		s := data[val]
		if !stringInSlice(header, timeHeaders) || s == "" {
			datum = s
		} else {
			if milli, ok := f(s); ok {
				datum = strconv.FormatInt(milli, 10)
			}
		}

		out[i] = datum
	}

	return out
}

func mapToCommon(headers []string, m map[string]string) map[string]int {
	out := map[string]int{}
	for i, header := range headers {
		h := m[strings.ToLower(header)]
		out[h] = i
	}
	return out
}

func MapKcmToUnix(str string) (int64, bool) {
	str = cleanKcmTime(str)
	var format string
	if string(str[11]) == "0" && string(str[12]) == "0" {
		format = kcmZeroHourTimeFormat
	} else {
		format = kcmTimeFormat
	}
	return mapToUnix(str, format)
}

func MapSfdToUnix(str string) (int64, bool) {
	//same formatting as kcm
	str = cleanKcmTime(str)
	var format string
	if string(str[11]) == "0" && string(str[12]) == "0" {
		format = sfdZeroHourTimeFormat
	} else {
		format = sfdTimeFormat
	}
	return mapToUnix(str, format)
}

func MapSpdToUnix(str string) (int64, bool) {
	//if starts with num, same format as kcm
	//otherwise use spdArrival format
	//reg := regexp.MustCompile("^([01])")
	var format string
	//if reg.MatchString(str) {
	str = cleanKcmTime(str)
	if string(str[11]) == "0" && string(str[12]) == "0" {
		format = spdZeroHourTimeFormat
	} else {
		format = spdTimeFormat
	}
	return mapToUnix(str, format)
	//}else{
	//	str = cleanSpdTime(str)
	//	if string(str[11]) == "0" && string(str[12]) == "0"{
	//		format = spdArrivalZeroHourTimeFormat
	//	}else{
	//		format = spdArrivalTimeFormat
	//	}
	//	return mapToUnix(str, format)
	//}
}

func mapToUnix(str, format string) (int64, bool) {
	t, err := time.Parse(format, str)

	if err != nil {
		log.Println(err)
		return 0, false
	} else {
		return t.UnixNano() / 1000000, true
	}
}

func cleanKcmTime(str string) string {
	reg := regexp.MustCompile("^[0-9]$")
	bySpace := strings.Split(str, " ")
	date := bySpace[0]
	t := bySpace[1]
	bySlashes := strings.Split(date, "/")
	byColon := strings.Split(t, ":")
	month := bySlashes[0]
	day := bySlashes[1]
	year := bySlashes[2]
	hour := byColon[0]
	minute := byColon[1]
	if reg.MatchString(month) {
		month = "0" + month
	}
	if reg.MatchString(day) {
		day = "0" + day
	}
	if reg.MatchString(hour) {
		hour = "0" + hour
	}
	if len(year) == 2 {
		year = "20" + year
	}

	return month + "/" + day + "/" + year + " " + hour + ":" + minute
}

func cleanSpdTime(str string) string {
	reg := regexp.MustCompile(":[0]{3}[A|P]M$")
	str = reg.ReplaceAllString(str, "")
	bySpaces := strings.Split(str, " ")
	month := monthMap[bySpaces[0]]
	day := bySpaces[1]
	year := bySpaces[2]
	t := bySpaces[3]
	return month + "/" + day + "/" + year + " " + t
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

var monthMap = map[string]string{
	"Jan": "01",
}
