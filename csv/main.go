package main

import (
	"github.com/tushar2708/altcsv"
	"log"
	"os"
)

func main() {
	file, err := os.Create("out.csv")
	checkError("Cannot create file", err)
	//TODO: handle file close error
	defer file.Close()

	writer := altcsv.NewWriter(file)
	writer.AllQuotes = true
	defer writer.Flush()

	err = writer.Write(allHeaders)
	checkError("Cannot write headers", err)

	files := []string{"../../quoted/kcm-01-2020.csv", "../../quoted/sfd-01-2020.csv", "../../quoted/spd-01-2020.csv", "../../quoted/wsp-01-2020.csv"}

	writeToFile(writer, files[0], MapKcmToCommon, MapKcmToUnix)
	writeToFile(writer, files[1], MapSfdToCommon, MapSfdToUnix)
	writeToFile(writer, files[2], MapSpdToCommon, MapSpdToUnix)
	writeToFile(writer, files[3], MapWspToCommon, MapKcmToUnix)

	log.Println("done")
}

func checkError(message string, err error) {
	if err != nil {
		log.Fatal(message, err)
	}
}
