package main

import (
	"bytes"
	"encoding/csv"
	"fmt"
	"github.com/tushar2708/altcsv"
	"io/ioutil"
	"log"
)

func writeToFile(writer *altcsv.Writer, filename string, toCommon func([]string)map[string]int, toUnix func(string) (int64, bool)) {
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		panic(err)
	}
	r := csv.NewReader(bytes.NewReader(b))
	r.Comma = ','
	r.Comment = '#'

	records, err := r.ReadAll()
	if err != nil {
		log.Fatal(err)
	}
	kcmHeaders := records[0]
	headerIndexMap := toCommon(kcmHeaders)

	for i, record := range records {
		if i == 0 {
			continue
		}

		row := Map(headerIndexMap, allHeaders, record, toUnix)
		err = writer.Write(row)
		checkError(fmt.Sprintf("Cannot write row %d", i), err)
	}
}
