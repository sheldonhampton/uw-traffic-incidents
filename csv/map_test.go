package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestMap(t *testing.T){
	headers := []string{"alfa", "bravo", "charlie", "delta", "echo"}
	data := []string{"J. Cole", "Jon Bellion", "Andre 3000", "Lupe Fiasco", "Lil Wayne"}
	masterHeaders := []string{"alfa", "charlie", "delta", "foxtrot", "juliet", "x-ray"}
	m := map[string]string{"alfa":"alfa", "bravo": "bravo", "charlie":"charlie", "delta":"juliet", "echo":"x-ray"}
	expectedData := []string{"J. Cole", "Andre 3000", "", "", "Lupe Fiasco", "Lil Wayne"}

	headerIndexMap := mapToCommon(headers, m)
	assert.Len(t, headerIndexMap, 5)
	assert.Equal(t, 0, headerIndexMap["alfa"])
	assert.Equal(t, 1, headerIndexMap["bravo"])
	assert.Equal(t, 2, headerIndexMap["charlie"])
	assert.Equal(t, 3, headerIndexMap["juliet"])
	assert.Equal(t, 4, headerIndexMap["x-ray"])
	row := Map(headerIndexMap, masterHeaders, data, func(s string) (int64, bool) {
		return int64(0), true
	})

	assert.Equal(t, len(expectedData), len(row))

	for i := range masterHeaders {
		assert.Equal(t, expectedData[i], row[i])
	}
}

func TestMapToCommon(t *testing.T) {
	headers := []string{"alfa", "bravo", "charlie", "delta", "echo"}
	m := map[string]string{"alfa":"alfa", "bravo": "bravo", "charlie":"charlie", "delta":"juliet", "echo":"x-ray"}

	headerIndexMap := mapToCommon(headers, m)
	assert.Len(t, headerIndexMap, 5)
	assert.Equal(t, 0, headerIndexMap["alfa"])
	assert.Equal(t, 1, headerIndexMap["bravo"])
	assert.Equal(t, 2, headerIndexMap["charlie"])
	assert.Equal(t, 3, headerIndexMap["juliet"])
	assert.Equal(t, 4, headerIndexMap["x-ray"])
}

func TestCleanTime(t *testing.T) {
	a, b := "1/17/20 1:16", "01/31/2020 23:51"
	assert.Equal(t, "01/17/2020 01:16", cleanKcmTime(a))
	assert.Equal(t, "01/31/2020 23:51", cleanKcmTime(b))
}

func TestMapToUnix(t *testing.T) {
	a, b := "1/31/2020 23:51", "01/31/2020 23:51"
	milli, ok := mapToUnix(a, "01/02/2006 15:04")
	assert.False(t, ok)
	assert.Equal(t, int64(0), milli)

	milli, ok = mapToUnix(b, "01/02/2006 15:04")
	assert.True(t, ok)
	assert.Equal(t, int64(1580514660), milli)
}

func TestStringInSlice(t *testing.T) {
	assert.True(t, stringInSlice("a", []string{"a","b"}))
	assert.False(t, stringInSlice("c", []string{"d", "e"}))
}