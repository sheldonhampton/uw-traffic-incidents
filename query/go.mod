module uw-traffic-query

go 1.14

require (
	github.com/aws/aws-lambda-go v1.17.0
	github.com/aws/aws-sdk-go v1.31.15 // indirect
	github.com/aws/aws-sdk-go-v2 v0.23.0
	github.com/stretchr/testify v1.5.1
)
