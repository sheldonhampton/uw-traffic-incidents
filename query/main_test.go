package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestBuildWhereClause(t *testing.T){
	none := map[string]string{}
	from := map[string]string{"from": "1234"}
	to := map[string]string{"to": "5678"}
	fromTo := map[string]string{"from": "1234", "to":"56789"}

	assert.Empty(t, buildWhereClause(none))
	assert.Equal(t, " WHERE CAST(start_date_time AS bigint) >= 1234", buildWhereClause(from))
	assert.Equal(t, " WHERE CAST(start_date_time AS bigint) <= 5678", buildWhereClause(to))
	assert.Equal(t, " WHERE CAST(start_date_time AS bigint) >= 1234 AND CAST(start_date_time AS bigint) <= 56789", buildWhereClause(fromTo))
}
