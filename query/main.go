package main

import (
	"context"
	"encoding/json"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/aws/external"
	"github.com/aws/aws-sdk-go-v2/service/athena"
	"log"
	"net/http"
	"strconv"
	"time"
)

type Incident struct {
	Id            string  `json:"id"`
	StartDateTime int64   `json:"startDateTime"`
	Latitude      float64 `json:"latitude"`
	Longitude     float64 `json:"longitude"`
	Type          string  `json:"type"`
}

const outputBucket = "s3://traffic-incidents-query-results/"

var client *athena.Client

func init() {
	cfg, err := external.LoadDefaultAWSConfig()
	if err != nil {
		log.Println("config error: ", err)
	}

	cfg.Region = "us-west-2"
	client = athena.New(cfg)
}

func HandleRequest(r events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	headers := map[string]string{
		"content-type":                "application/json",
		"access-control-allow-origin": "*",
		"accept":                      "*/*",
	}

	query := "SELECT id, start_date_time, latitude, longitude, type FROM db.mapped" + buildWhereClause(r.QueryStringParameters) + " LIMIT 50;"

	// Execute an Athena Query
	QueryID := executeQuery(query)
	if QueryID == nil {
		return events.APIGatewayProxyResponse{
			StatusCode: http.StatusBadRequest,
			Headers:    headers,
			Body:       "",
		}, nil
	}

	//TODO: manage with channel
	// Wait for some time for query completion
	time.Sleep(10 * time.Second) // Otherwise create a loop and try for every x seconds
	res, err := getQueryResults(QueryID)

	if err != nil {
		log.Println("Error getting Query Response: ", err)
		return events.APIGatewayProxyResponse{
			StatusCode: http.StatusInternalServerError,
			Headers:    headers,
			Body:       "",
		}, nil
	}

	//we don't need the column headers
	incs := extractIncidents(res.ResultSet.Rows[1:])
	b, err := json.Marshal(incs)
	if err != nil {
		log.Println("could not marshal incidents ", err)
		return events.APIGatewayProxyResponse{
			StatusCode: http.StatusInternalServerError,
			Headers:    headers,
			Body:       "",
		}, nil
	}

	return events.APIGatewayProxyResponse{
		StatusCode: http.StatusOK,
		Headers:    headers,
		Body:       string(b),
	}, nil
}

//Execute the query and return the query ID
func executeQuery(query string) *string {
	resultConf := &athena.ResultConfiguration{
		OutputLocation: aws.String(outputBucket),
	}

	params := &athena.StartQueryExecutionInput{
		QueryString:         aws.String(query),
		ResultConfiguration: resultConf,
	}

	req := client.StartQueryExecutionRequest(params)

	res, err := req.Send(context.TODO())

	if err != nil {
		log.Println("Query Error: ", err)
		return nil
	}

	return res.QueryExecutionId
}

// Takes queryId as input and returns its response
func getQueryResults(QueryID *string) (*athena.GetQueryResultsResponse, error) {
	params1 := &athena.GetQueryResultsInput{
		QueryExecutionId: QueryID,
	}
	req := client.GetQueryResultsRequest(params1)

	resp, err := req.Send(context.TODO())

	if err != nil {
		log.Println("Query Response Error: ", err)
		return nil, err
	}

	return resp, nil
}

func extractIncidents(rows []athena.Row) []Incident {
	out := make([]Incident, len(rows))

	for i, row := range rows {
		var inc Incident

		id := *row.Data[0].VarCharValue
		if id == "" {
			continue
		}

		milli, err := strconv.ParseInt(*row.Data[1].VarCharValue, 10, 64)
		if err != nil {
			log.Println("error getting start time from ", *row.Data[1].VarCharValue, err)
			continue
		}

		lat, err := strconv.ParseFloat(*row.Data[2].VarCharValue, 64)
		if err != nil {
			log.Println("error getting latitude ", *row.Data[2].VarCharValue, err)
			continue
		}

		long, err := strconv.ParseFloat(*row.Data[3].VarCharValue, 64)
		if err != nil {
			log.Println("error getting longitude ", *row.Data[3].VarCharValue, err)
			continue
		}

		inc.Id = id
		inc.StartDateTime = milli
		inc.Latitude = lat
		inc.Longitude = long
		//Incident.Type can be empty
		inc.Type = *row.Data[4].VarCharValue

		out[i] = inc
	}

	return out
}

func buildWhereClause(m map[string]string) string {
	from, fok := m["from"]
	to, tok := m["to"]

	if !fok && !tok {
		return ""
	}

	out := " WHERE "
	var fStr string
	var tStr string

	if fok {
		fStr = "CAST(start_date_time AS bigint) >= " + from
		if tok {
			tStr = "CAST(start_date_time AS bigint) <= " + to
			out += fStr + " AND " + tStr
		} else {
			out += fStr
		}
	} else {
		tStr = "CAST(start_date_time AS bigint) <= " + to
		out += tStr
	}

	return out
}

func main() {
	lambda.Start(HandleRequest)
}
